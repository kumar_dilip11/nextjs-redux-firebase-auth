const prodConfig = {
  apiKey: 'AIzaSyBD66hplKBABV5we0nIgaQIqi5FX_MLtXE',
  authDomain: 'todo-ccfb9.firebaseapp.com'
}

const devConfig = {
  apiKey: '',
  authDomain: ''
}

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

export default config
